FROM sleepykush/docker:librenms

EXPOSE 80 
EXPOSE 514
EXPOSE  514/UDP


CMD /etc/init.d/snmpd restart && /etc/init.d/php7.2-fpm start && /etc/init.d/mysql start && /etc/init.d/nginx start && bash

