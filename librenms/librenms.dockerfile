FROM ubuntu:18.04

ENV mysql_user_root="root" 
ENV mysql_user="librenms" 
ENV mysql_pass="librenm" 
ENV mysql_db="librenms" 
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt install -y software-properties-common; add-apt-repository universe; apt update; apt install -y curl wget  composer fping git graphviz imagemagick mariadb-client mariadb-server mtr-tiny nginx-full nmap php7.2-cli php7.2-curl php7.2-fpm php7.2-gd php7.2-json php7.2-mbstring php7.2-mysql php7.2-snmp php7.2-xml php7.2-zip python-memcache python-mysqldb rrdtool snmp snmpd whois acl vim iputils-ping bash 

RUN useradd librenms -d /opt/librenms -M -r && usermod -a -G librenms www-data
WORKDIR /opt
ADD librenms /opt/librenms
RUN chmod 0775 /opt/librenms
RUN chown -R librenms:librenms /opt/librenms; chmod 774 /opt/librenms; setfacl -d -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/; setfacl -R -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/

RUN cd /etc/ssl/certs ; wget http://curl.haxx.se/ca/cacert.pem --no-check-certificate


RUN sed -i '/\i[openssl\]/a openssl.cafile=/etc/ssl/certs/cacert.pem' /etc/php/7.2/cli/php.ini;sed -i '/\i[openssl\]/a openssl.capath=/etc/ssl/certs' /etc/php/7.2/cli/php.ini; sed -i '/\i[openssl\]/a openssl.cafile=/etc/ssl/certs/cacert.pem' /etc/php/7.2/fpm/php.ini;sed -i '/\i[openssl\]/a openssl.capath=/etc/ssl/certs' /etc/php/7.2/fpm/php.ini

RUN /etc/init.d/php7.2-fpm start && composer create-project --no-dev --keep-vcs librenms/librenms librenms dev-master
RUN /etc/init.d/mysql start && sleep 3 && mysql -e "CREATE DATABASE librenms CHARACTER SET utf8 COLLATE utf8_unicode_ci; CREATE USER 'librenms'@'localhost' IDENTIFIED BY 'librenms'; GRANT ALL PRIVILEGES ON librenms.* TO 'librenms'@'localhost identified by 'librenms'; FLUSH PRIVILEGES; \q"

RUN sed -i '/\[mysqld\]/a innodb_file_per_table=1' /etc/mysql/mariadb.conf.d/50-server.cnf; sed -i '/innodb_file_per_table=1/a lower_case_table_names=0' /etc/mysql/mariadb.conf.d/50-server.cnf; sed -i '/\[Date\]/a date.timezone = Asia/Kolkata' /etc/php/7.2/fpm/php.ini; sed -i '/\[Date\]/a date.timezone = Asia/Kolkata' /etc/php/7.2/cli/php.ini; 


ADD librenms.conf etc/nginx/conf.d/librenms.conf
RUN rm /etc/nginx/sites-enabled/default; cp /opt/librenms/snmpd.conf.example /etc/snmp/snmpd.conf; curl -o -k /usr/bin/distro https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/distro; chmod +x /usr/bin/distro;/etc/init.d/snmpd start

RUN cp /opt/librenms/librenms.nonroot.cron /etc/cron.d/librenms; cp /opt/librenms/misc/librenms.logrotate /etc/logrotate.d/librenms
RUN chown -R librenms:librenms /opt/librenms; chown librenms:librenms /opt/librenms/config.php

EXPOSE 80 514 514/udp
CMD /etc/init.d/mysql start && /etc/init.d/php7.2-fpm start && /etc/init.d/nginx start && /etc/init.d/snmpd start && bash 













 






